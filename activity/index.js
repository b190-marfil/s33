/*https://jsonplaceholder.typicode.com/todos*/


fetch('https://jsonplaceholder.typicode.com/todos')
.then(response => response.json())
.then(json => {
   	let titles = json.map((value) => {
   	    return value.title 
   	})
   	console.log(titles);
})

fetch("https://jsonplaceholder.typicode.com/todos/1")
.then(response => response.json())
.then(json => console.log(json));



fetch('https://jsonplaceholder.typicode.com/todos',{
	method: "POST",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		completed: false,
		id: 201,
		title: "Created To Do List Item",
		userId:1
	})
})
.then(response => response.json())
.then(json => console.log(json))


fetch('https://jsonplaceholder.typicode.com/todos/1',{
	method: "PUT",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		dateCompleted: "Pending",
		description: "To update the my to do list with a different data structure",
		id: 1,
		status: "Pending",
		title: "Updated To  Do List Item",
		userId: 1
	})
})
.then(response => response.json())
.then(json => console.log(json))


fetch("https://jsonplaceholder.typicode.com/todos/1",{
	method: "PATCH",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		dateCompleted: "07/09/21",
		status: "Complete"
	})
})
.then(response => response.json())
.then(json => console.log(json))

fetch("https://jsonplaceholder.typicode.com/todos/1",{
	method: "DELETE",
})

